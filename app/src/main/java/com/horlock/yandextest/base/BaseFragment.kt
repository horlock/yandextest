package com.horlock.yandextest.base

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.PermissionChecker
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.commit
import com.horlock.yandextest.R
import com.horlock.yandextest.ui.MainActivity
import com.horlock.yandextest.utils.hideKeyboard

abstract class BaseFragment(@LayoutRes val layoutId: Int) : Fragment(layoutId) {

    private val parentLayoutId = R.id.container
    lateinit var viewModel: BaseViewModel
    private var lastStatusBarColor = 0

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        onCreateMapView(savedInstanceState)
        lastStatusBarColor = requireActivity().window.statusBarColor
        if((activity as MainActivity).viewModel != null)
            viewModel = (activity as MainActivity).viewModel!!

        initialize()
    }

    open fun onCreateMapView(savedInstanceState: Bundle?) {
    }

    abstract fun initialize()

    fun addFragment(
        fragment: BaseFragment,
        addBackStack: Boolean = true,
        @IdRes id: Int = parentLayoutId,
        tag: String = "base_fragment_tag"
    ) {
        hideKeyboard(view)

        activity?.supportFragmentManager?.commit {
            if (addBackStack) addToBackStack(fragment.hashCode().toString())
            setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
            add(id, fragment, tag)
        }
    }

    fun replaceFragment(fragment: BaseFragment, addBackStack: Boolean = false,
                        @IdRes id: Int = parentLayoutId, tag : String = "base_fragment_tag") {
        activity?.supportFragmentManager?.commit {
            if(addBackStack) addToBackStack(fragment.hashCode().toString())
            replace(id, fragment)
        }
    }

    fun showMessage(text: String){
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
    }

    fun deleteFragment(tag : String){
        val manager = activity?.supportFragmentManager

        if(manager?.fragments?.get(manager.backStackEntryCount - 1)?.tag == tag){
            manager.popBackStackImmediate()
        }else{
            manager?.fragments?.forEach {
                if(it.tag == tag){
                    manager.beginTransaction().remove(it).commit()
                }
            }
        }
    }

    fun isGranted(permission: String) = run {
        (PermissionChecker.checkSelfPermission(requireContext(), permission
        ) == PermissionChecker.PERMISSION_GRANTED)
    }

    fun makeTransparent(isTransparent : Boolean){
        if(isTransparent)
            requireActivity().window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        else requireActivity().window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
    }



    fun finishFragment() {
        hideKeyboard(requireView())
        activity?.supportFragmentManager?.popBackStackImmediate()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        hideKeyboard(view)
        if (lastStatusBarColor != 0)
            requireActivity().window.statusBarColor = lastStatusBarColor
    }

}

fun AppCompatActivity.initialFragment(fragment: BaseFragment) {
    supportFragmentManager.beginTransaction().add(R.id.container, fragment).commit()
}

fun AppCompatActivity.initialFragment(layoutId: Int, fragment: BaseFragment) {
    supportFragmentManager.beginTransaction().add(layoutId, fragment).commit()
}

fun AppCompatActivity.initialFragmentWithBackStack(fragment: BaseFragment) {
    supportFragmentManager
        .beginTransaction().add(R.id.container, fragment)
        .addToBackStack(fragment.tag)
        .commit()
}

fun FragmentActivity.addFragmentWithFinishPrevious(
    fragment: BaseFragment,
    addBackStack: Boolean = true,
    @IdRes id: Int = R.id.container,
    tag : String = "base_fragment_tag"
) {
    finishFragment()
    supportFragmentManager.commit {
        if (addBackStack) addToBackStack(fragment.hashCode().toString())
        setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
        add(id, fragment, tag)
    }
}

fun FragmentActivity.replaceFragmentWithFinishPrevious(
    fragment: BaseFragment,
    addBackStack: Boolean = false,
    @IdRes id: Int = R.id.container,
    tag : String = "base_fragment_tag"
) {
    finishFragment()
    supportFragmentManager.commit {
        if (addBackStack) addToBackStack(fragment.hashCode().toString())
        setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
        replace(id, fragment, tag)
    }
}

fun FragmentActivity.finishFragment() {
    supportFragmentManager.popBackStackImmediate()
}