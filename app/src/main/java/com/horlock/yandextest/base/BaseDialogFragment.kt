package com.horlock.yandextest.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.IdRes
import androidx.annotation.LayoutRes
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.commit
import com.horlock.yandextest.R
import com.horlock.yandextest.ui.MainActivity
import com.horlock.yandextest.utils.hideKeyboard

abstract class BaseDialogFragment(@LayoutRes val layoutId: Int) : DialogFragment() {

    lateinit var viewModel: BaseViewModel
    private val parentLayoutId = R.id.container

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(layoutId, container, false)
    }

    override fun getTheme(): Int {
        return R.style.DialogTheme
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dialog?.window?.setBackgroundDrawableResource(android.R.color.transparent)

        if ((activity as MainActivity).viewModel != null)
            viewModel = (activity as MainActivity).viewModel!!

        initialize()
    }

    abstract fun initialize()

    fun addFragment(
        fragment: BaseFragment,
        addBackStack: Boolean = true,
        @IdRes id: Int = parentLayoutId,
        tag: String = "base_fragment_tag"
    ) {
        hideKeyboard(view)
        activity?.supportFragmentManager?.commit {
            if (addBackStack) addToBackStack(fragment.hashCode().toString())
            setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
            add(id, fragment)
        }
    }
}