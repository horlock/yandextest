package com.horlock.yandextest.base

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.horlock.yandextest.room.Bookmark
import com.horlock.yandextest.repo.BookmarkRepository
import com.yandex.mapkit.GeoObjectCollection

/**
 * Developed by horlock
 */

open class BaseViewModel(app: Application) : AndroidViewModel(app) {

    private var repository: BookmarkRepository = BookmarkRepository(app)

    val searchResult = MutableLiveData<GeoObjectCollection.Item>()

    val selectedBookmark = MutableLiveData<Bookmark>()

    fun getBookmarks() = repository.getBookmarks()

    fun setBookmark(bookmark: Bookmark) { repository.setBookmark(bookmark) }

    fun deleteBookmark(bookmark: Bookmark) { repository.deleteBookmark(bookmark)}
}
