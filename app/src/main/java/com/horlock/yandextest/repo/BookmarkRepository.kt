package com.horlock.yandextest.repo

import android.app.Application
import com.horlock.yandextest.room.Bookmark
import com.horlock.yandextest.room.BookmarkDao
import com.horlock.yandextest.room.BookmarkDatabase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext

class BookmarkRepository(app: Application): CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main

    private var bookmarkDao : BookmarkDao? = null

    init {
        val db = BookmarkDatabase.getDatabase(app)
        bookmarkDao = db?.bookmarkDao()
    }

    fun getBookmarks() = bookmarkDao?.getBookmarks()

    fun setBookmark(bookmark: Bookmark){
        launch {
            setBookmarkBG(bookmark)
        }
    }

    fun deleteBookmark(bookmark: Bookmark){
        launch {
            deleteBookmarkBG(bookmark)
        }
    }

    private suspend fun setBookmarkBG(bookmark: Bookmark){
        withContext(Dispatchers.IO){
            bookmarkDao?.insert(bookmark)
        }
    }

    private suspend fun deleteBookmarkBG(bookmark: Bookmark){
        withContext(Dispatchers.IO){
            bookmarkDao?.delete(bookmark)
        }
    }
}