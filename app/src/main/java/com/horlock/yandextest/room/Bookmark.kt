package com.horlock.yandextest.room

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "bookmarks")
class Bookmark() {

    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
    var name : String? = null
    var address : String? = null
    var latitude: Double? = 0.0
    var longitude: Double? = 0.0
    var category: String? = null
    var workingHours: String? = null
    var rating: Float? = null
    var reviews: Int? = null
}
