package com.horlock.yandextest.room

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface BookmarkDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(bookmark: Bookmark)

    @Query("SELECT * FROM bookmarks")
    fun getBookmarks(): LiveData<List<Bookmark>>

    @Delete
    fun delete(bookmark: Bookmark)
}