package com.horlock.yandextest.ui.screens.search

import android.widget.Toast
import com.horlock.yandextest.R
import com.horlock.yandextest.base.BaseDialogFragment
import com.horlock.yandextest.room.Bookmark
import kotlinx.android.synthetic.main.screen_delete_bookmark.*

class DeleteBookmarkScreen(val bookmark: Bookmark) : BaseDialogFragment(R.layout.screen_delete_bookmark) {

    var deleted : ((Any) -> Unit) ?= null

    override fun initialize() {

        name.text = bookmark.name

        backFrame.setOnClickListener {
            dismiss()
        }

        cancel.setOnClickListener {
            dismiss()
        }

        delete.setOnClickListener {
            viewModel.deleteBookmark(bookmark)
            Toast.makeText(requireContext(),getString(R.string.deleted), Toast.LENGTH_SHORT).show()
            deleted?.invoke(Any())
            dismiss()
        }
    }
}