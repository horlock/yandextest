package com.horlock.yandextest.ui.screens.profile

import com.horlock.yandextest.R
import com.horlock.yandextest.base.BaseFragment

class UserProfileScreen : BaseFragment(R.layout.screen_user_profile) {

    override fun initialize() {
        makeTransparent(false)
    }
}