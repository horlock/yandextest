package com.horlock.yandextest.ui.screens.search

import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.style.TextAppearanceSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.horlock.yandextest.R
import com.yandex.mapkit.GeoObjectCollection
import kotlinx.android.synthetic.main.item_search_result.view.*
import java.util.*


class SearchResultsAdapter() : RecyclerView.Adapter<ViewHolder>() {

    var results = mutableListOf<GeoObjectCollection.Item>()
    var filter : String = ""
    var searchResultClicked: ((GeoObjectCollection.Item) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_search_result, parent, false))
    }

    override fun getItemCount(): Int {
        return results.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val result = results[position]

        val fullText = result.obj?.name

        if(filter.isEmpty()){
            holder.itemView.name.text = fullText
        }else {
            val startPos =
                fullText?.toLowerCase(Locale.US)?.indexOf(filter.toLowerCase(Locale.US)) ?: -1
            val endPos = startPos.plus(filter.length)

            if (startPos != -1) {
                val spannable: Spannable = SpannableString(fullText)
                val blueColor =
                    ColorStateList(arrayOf(intArrayOf()), intArrayOf(Color.BLACK))
                val highlightSpan =
                    TextAppearanceSpan(null, Typeface.BOLD, -1, blueColor, null)
                spannable.setSpan(
                    highlightSpan,
                    startPos,
                    endPos,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
                holder.itemView.name.text = spannable
            } else {
                holder.itemView.name.text = fullText
            }
        }
        holder.itemView.address.text = result.obj?.descriptionText

        holder.itemView.setOnClickListener {
            searchResultClicked?.invoke(result)
        }
    }

}

class ViewHolder(view: View): RecyclerView.ViewHolder(view)