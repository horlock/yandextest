package com.horlock.yandextest.ui.screens.addressbook

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.horlock.yandextest.R
import com.horlock.yandextest.room.Bookmark
import com.horlock.yandextest.ui.screens.search.ViewHolder
import kotlinx.android.synthetic.main.item_bookmark.view.*

class AddressesAdapter : RecyclerView.Adapter<ViewHolder>() {

    var bookmarks: List<Bookmark> = emptyList()
    var bookmarkClicked: ((Bookmark) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_bookmark, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return bookmarks.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemView = holder.itemView
        val bookmark = bookmarks[position]

        itemView.name.text = bookmark.name
        itemView.address.text = bookmark.address

        if(bookmark.workingHours == null && bookmark.rating == null){
            itemView.addInfoHolder.visibility = View.GONE
        }else {
            if (bookmark.rating != null) {
                itemView.rating.visibility = View.VISIBLE
                itemView.ratingBar.visibility = View.VISIBLE
                itemView.rating.text = bookmark.rating.toString()
                itemView.ratingBar.rating = bookmark.rating?.toFloat()!!
            } else {
                itemView.rating.visibility = View.GONE
                itemView.ratingBar.visibility = View.GONE
            }

            if (bookmark.workingHours != null) {
                itemView.wokringHours.visibility = View.VISIBLE
                itemView.wokringHours.text = bookmark.workingHours
            } else {
                itemView.wokringHours.visibility = View.GONE
            }
        }

        itemView.setOnClickListener {
            bookmarkClicked?.invoke(bookmark)
        }
    }

}