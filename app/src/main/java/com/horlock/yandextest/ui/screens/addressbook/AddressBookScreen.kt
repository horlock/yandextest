package com.horlock.yandextest.ui.screens.addressbook

import android.view.View
import androidx.lifecycle.Observer
import com.horlock.yandextest.R
import com.horlock.yandextest.base.BaseFragment
import com.horlock.yandextest.ui.MainActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.screen_address_book.*

class AddressBookScreen : BaseFragment(R.layout.screen_address_book) {

    override fun initialize() {
        makeTransparent(false)

        val adapter = AddressesAdapter()
        addresses.adapter = adapter

        viewModel.getBookmarks()?.observe(this, Observer {
            if (isResumed) {
                adapter.bookmarks = it
                adapter.notifyDataSetChanged()

                if(it.isNotEmpty())
                    emptyHolder.visibility = View.GONE
            }
        })

        adapter.bookmarkClicked = {
            viewModel.selectedBookmark.value = it
            (requireActivity() as MainActivity).bottomNavigation.selectedItemId = R.id.loc
        }
    }
}