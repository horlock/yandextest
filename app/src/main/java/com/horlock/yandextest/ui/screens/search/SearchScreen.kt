package com.horlock.yandextest.ui.screens.search

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.TimeInterpolator
import android.annotation.SuppressLint
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import android.widget.ImageView
import androidx.lifecycle.Observer
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.horlock.yandextest.R
import com.horlock.yandextest.base.BaseFragment
import com.horlock.yandextest.room.Bookmark
import com.horlock.yandextest.ui.MainActivity
import com.horlock.yandextest.utils.convertDpToPixel
import com.yandex.mapkit.Animation
import com.yandex.mapkit.GeoObjectCollection
import com.yandex.mapkit.MapKitFactory
import com.yandex.mapkit.geometry.Point
import com.yandex.mapkit.map.CameraPosition
import com.yandex.mapkit.search.BusinessObjectMetadata
import com.yandex.mapkit.search.BusinessRating1xObjectMetadata
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.screen_map.*
import kotlinx.android.synthetic.main.view_address_detail.*
import kotlinx.android.synthetic.main.view_address_detail.view.*


class SearchScreen : BaseFragment(R.layout.screen_map) {

    private var isPointerStopped = true
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var location: Location? = null
    private lateinit var detailsView: View

    override fun initialize() {
        makeTransparent(true)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())

        detailsView = LayoutInflater.from(requireContext())
            .inflate(R.layout.view_address_detail, null)

        moveCameraToCurrentLocation(0f)

        locationBtn.setOnClickListener {
            moveCameraToCurrentLocation(1f)
        }

        search.setOnClickListener {
            SearchResultsScreen(mapView.map.visibleRegion)
                .show(requireActivity().supportFragmentManager, "search_results_screen")
        }

        mapView.map.addCameraListener { map, cameraPosition, cameraUpdateReason, b ->
            if(b){
                isPointerStopped = true
            }else{
                if(isPointerStopped){
                    if(animationEnded)
                        levitate(pinTop, -100f, true)
                    isPointerStopped = false
                }
            }
        }

        viewModel.searchResult.observe(this, Observer {
            if(isResumed){
                moveCamera(it.obj?.geometry?.get(0)?.point
                    ?: Point(0.0,0.0), 1f)

                showLocationDetails(it)
            }
        })

        val params = search.layoutParams as ViewGroup.MarginLayoutParams
        search.layoutParams = params.apply {
            setMargins(params.leftMargin, getStatusBarHeight() +
                    requireActivity().convertDpToPixel(16f),
                params.rightMargin, params.bottomMargin)
        }

        Handler().postDelayed({
            val bookmark = viewModel.selectedBookmark.value
            if(bookmark != null){
                moveCamera(Point(bookmark.latitude!!, bookmark.longitude!!), 1f)

                showLocationDetails(bookmark)
            }
        }, 200)
    }

    private fun moveCamera(point: Point, duration: Float){
        mapView.map.move(CameraPosition(
            point,
            18.0f,
            0.0f,
            0.0f
        ),
            Animation(Animation.Type.SMOOTH, duration),
            null)
    }

    private fun showLocationDetails(item: GeoObjectCollection.Item) {
        (requireActivity() as MainActivity).container.addView(detailsView)
        detailsView.bringToFront()

        val businessInfo = item
            .obj?.metadataContainer?.getItem(BusinessObjectMetadata::class.java)

        val ratingInfo = item
            .obj?.metadataContainer?.getItem(BusinessRating1xObjectMetadata::class.java)

        detailsView.name.text = item.obj?.name
        detailsView.category.text = createCategoryList(businessInfo)

        if(businessInfo != null && businessInfo.workingHours != null){
            detailsView.wokringHours.text = businessInfo.workingHours?.text
        }else{
            detailsView.wokringHours.text = getString(R.string.empty_working_hours)
        }

        if(ratingInfo != null){
            detailsView.addInfoHolder.visibility = View.VISIBLE
            detailsView.rating.text = ratingInfo.score.toString()
            detailsView.ratingBar.rating = ratingInfo.score ?: 0f
            detailsView.reviews.text = (ratingInfo.reviews.toString() + " отзывов")
        }else{
            detailsView.addInfoHolder.visibility = View.GONE
        }

        detailsView.close.setOnClickListener {
            (requireActivity() as MainActivity).container.removeView(detailsView)
        }

        detailsView.apply.visibility = View.VISIBLE
        detailsView.delete.visibility = View.GONE

        detailsView.apply.setOnClickListener {
            val bookmark = Bookmark()
            bookmark.name = item.obj?.name
            bookmark.address = item.obj?.descriptionText
            bookmark.category = createCategoryList(businessInfo)
            bookmark.latitude = item.obj?.geometry?.get(0)?.point?.latitude
            bookmark.longitude = item.obj?.geometry?.get(0)?.point?.longitude
            bookmark.rating = ratingInfo?.score
            bookmark.reviews = ratingInfo?.reviews

            val fragment = AddBookmarkScreen(bookmark)
            fragment.saved = {
                (requireActivity() as MainActivity).container.removeView(detailsView)
            }
            fragment.show(requireActivity().supportFragmentManager, "add_bookmark_screen")
        }
    }

    private fun showLocationDetails(item: Bookmark) {
        (requireActivity() as MainActivity).container.addView(detailsView)
        detailsView.bringToFront()

        detailsView.name.text = item.name
        detailsView.category.text = item.category

        if(item.workingHours != null){
            detailsView.wokringHours.text = item.workingHours
        }else{
            detailsView.wokringHours.text = getString(R.string.empty_working_hours)
        }

        if(item.rating != null){
            detailsView.addInfoHolder.visibility = View.VISIBLE
            detailsView.rating.text = item.rating.toString()
            detailsView.ratingBar.rating = item.rating ?: 0f
            detailsView.reviews.text = (item.reviews.toString() + " отзывов")
        }else{
            detailsView.addInfoHolder.visibility = View.GONE
        }

        detailsView.close.setOnClickListener {
            (requireActivity() as MainActivity).container.removeView(detailsView)
            viewModel.selectedBookmark.value = null
        }

        detailsView.apply.visibility = View.GONE
        detailsView.delete.visibility = View.VISIBLE

        detailsView.delete.setOnClickListener {
            val fragment = DeleteBookmarkScreen(item)
            fragment.deleted = {
                (requireActivity() as MainActivity).container.removeView(detailsView)
            }
            fragment.show(requireActivity().supportFragmentManager, "delete_bookmark_screen")
        }
    }

    private fun createCategoryList(businessInfo : BusinessObjectMetadata?) : String{
        var category = ""
        businessInfo?.categories?.forEachIndexed { index, it ->
            category = category.plus(it.name)
            if(index != businessInfo.categories.size - 1)
                category = category.plus(",")
        }

        if(category == "")
            return ("Неизвестно")

        return category
    }

    private fun getStatusBarHeight(): Int {
        var result = 0
        val resourceId = resources.getIdentifier(
            "status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            result = resources.getDimensionPixelSize(resourceId)
        }
        return result
    }

    @SuppressLint("MissingPermission")
    fun moveCameraToCurrentLocation(duration : Float){
        if (isGranted(android.Manifest.permission.ACCESS_FINE_LOCATION) &&
            isGranted(android.Manifest.permission.ACCESS_COARSE_LOCATION)
        ) {
            fusedLocationClient.lastLocation?.addOnSuccessListener {
                location = it
                mapView.map.move(
                    CameraPosition(
                        Point(it.latitude, it.longitude),
                        18.0f, 0.0f, 0.0f
                    ),
                    Animation(Animation.Type.SMOOTH, duration),
                    null
                )
            }
        }else{
            requestPermissions(arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                MainActivity.PERMISSION_REQUEST_CODE)
        }
    }

    var animationEnded = true
    fun levitate(movableView: View, Y: Float, animated: Boolean) {
        if (animated) {
            animationEnded = false
            val duration: Long = 800
            val interpolator: TimeInterpolator = DecelerateInterpolator()
            movableView.animate().translationYBy(Y).setDuration(duration)
                .setInterpolator(interpolator).setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        super.onAnimationEnd(animation)
                        if (!isPointerStopped) {
                            levitate(movableView, -Y, true)
                        }else{
                            if(Y < 0){
                                levitate(movableView, -Y, true)
                            }else{
                                animationEnded = true
                            }
                        }
                    }
                })
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MapKitFactory.setApiKey("522fb9ba-acc3-4c2a-ad64-371448cace44")
        MapKitFactory.initialize(requireContext())
    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
        MapKitFactory.getInstance().onStart()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
        MapKitFactory.getInstance().onStop()
        viewModel.selectedBookmark.value = null
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == MainActivity.PERMISSION_REQUEST_CODE){
            if(isGranted(android.Manifest.permission.ACCESS_FINE_LOCATION))
                moveCameraToCurrentLocation(0f)
        }
    }
}

