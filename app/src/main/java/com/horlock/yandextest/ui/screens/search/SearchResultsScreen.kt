package com.horlock.yandextest.ui.screens.search

import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import androidx.core.widget.doAfterTextChanged
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.horlock.yandextest.R
import com.horlock.yandextest.base.RoundedBottomSheetDialogFragment
import com.horlock.yandextest.utils.showKeyboard
import com.yandex.mapkit.map.VisibleRegion
import com.yandex.mapkit.map.VisibleRegionUtils
import com.yandex.mapkit.search.*
import com.yandex.runtime.Error
import com.yandex.runtime.network.NetworkError
import com.yandex.runtime.network.RemoteError
import kotlinx.android.synthetic.main.screen_search_results.*
import java.util.*


class SearchResultsScreen(val visibleRegion: VisibleRegion) :
    RoundedBottomSheetDialogFragment(R.layout.screen_search_results), Session.SearchListener {

    private lateinit var searchManager: SearchManager
    private var searchSession: Session? = null
    private lateinit var adapter: SearchResultsAdapter

    override fun initialize() {

        searchManager = SearchFactory.getInstance().createSearchManager(SearchManagerType.COMBINED)
        adapter = SearchResultsAdapter()

        adapter.searchResultClicked = {
            viewModel.searchResult.value = it
            dismiss()
        }

        var timer = Timer()

        search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(it: Editable?) {
                timer = Timer()

                timer.schedule(object : TimerTask() {
                    override fun run() {
                        requireActivity().runOnUiThread {
                            submitQuery(it.toString())
                        }
                    }
                }, 600)
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                timer.cancel()
            }

        })

        clear.setOnClickListener {
            search.setText("")
        }

        val layoutManager = LinearLayoutManager(
            requireContext(),
            LinearLayoutManager.VERTICAL, false
        )
        results.layoutManager = layoutManager
        results.addItemDecoration(
            DividerItemDecoration(
                requireContext(),
                layoutManager.orientation
            )
        )
        results.adapter = adapter

        Handler().postDelayed({

            search.requestFocus()
            showKeyboard(search)

        }, 1000)

    }

    private fun submitQuery(query: String) {
        if (query.length > 1) {
            val options = SearchOptions()
            options.snippets = Snippet.BUSINESS_RATING1X.value
            searchSession = searchManager.submit(
                query,
                VisibleRegionUtils.toPolygon(visibleRegion),
                options,
                this
            )

            adapter.filter = query
        }
    }

    override fun onSearchError(error: Error) {
        var errorMessage = getString(R.string.unknown_error_message)
        if (error is RemoteError) {
            errorMessage = getString(R.string.remote_error_message)
        } else if (error is NetworkError) {
            errorMessage = getString(R.string.network_error_message)
        }

        Toast.makeText(requireContext(), errorMessage, Toast.LENGTH_SHORT).show()
    }

    override fun onSearchResponse(resp: Response) {
        adapter.results = resp.collection.children
        adapter.notifyDataSetChanged()

        //resp.collection.children[0]
        // .obj.metadataContainer.getItem(BusinessRating1xObjectMetadata::class.java)
    }
}