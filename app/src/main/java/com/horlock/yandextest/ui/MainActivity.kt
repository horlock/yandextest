package com.horlock.yandextest.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.horlock.yandextest.R
import com.horlock.yandextest.base.BaseViewModel
import com.horlock.yandextest.base.initialFragment
import com.horlock.yandextest.ui.screens.addressbook.AddressBookScreen
import com.horlock.yandextest.ui.screens.search.SearchScreen
import com.horlock.yandextest.ui.screens.profile.UserProfileScreen
import com.horlock.yandextest.utils.isGranted
import com.horlock.yandextest.utils.replaceFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        val PERMISSION_REQUEST_CODE = 234
    }

    var viewModel: BaseViewModel? = null
    private var previousItem : Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProvider(this)[BaseViewModel::class.java]

        val permission = android.Manifest.permission.ACCESS_FINE_LOCATION

        if(!isGranted(permission)){
            requestPermissions(arrayOf(permission), PERMISSION_REQUEST_CODE)
        }

        initialFragment(R.id.mainContainer,
            SearchScreen()
        )

        bottomNavigation.selectedItemId = R.id.loc

        bottomNavigation.setOnNavigationItemSelectedListener {
            when(it.itemId){
                R.id.bookmark -> {
                    if(previousItem != it.itemId){
                        replaceFragment(AddressBookScreen(), false, R.id.mainContainer)
                    }
                }
                R.id.loc -> {
                    if(previousItem != it.itemId){
                        replaceFragment(SearchScreen(), false, R.id.mainContainer)
                    }
                }
                R.id.profile -> {
                    if(previousItem != it.itemId){
                        replaceFragment(UserProfileScreen(), false, R.id.mainContainer)
                    }
                }
            }
            previousItem = it.itemId
            return@setOnNavigationItemSelectedListener true
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        supportFragmentManager.fragments.forEach {
            it.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        supportFragmentManager.fragments.forEach {
            it.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }
}