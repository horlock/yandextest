package com.horlock.yandextest.ui.screens.search

import android.widget.Toast
import com.horlock.yandextest.R
import com.horlock.yandextest.base.BaseDialogFragment
import com.horlock.yandextest.room.Bookmark
import com.horlock.yandextest.utils.showKeyboard
import kotlinx.android.synthetic.main.screen_add_bookmark.*

class AddBookmarkScreen(val bookmark: Bookmark) : BaseDialogFragment(R.layout.screen_add_bookmark) {

    var saved: ((Any) -> Unit) ?= null

    override fun initialize() {

        editName.setText(bookmark.name)

        backFrame.setOnClickListener {
            dismiss()
        }

        cancel.setOnClickListener {
            dismiss()
        }

        save.setOnClickListener {
            bookmark.name = editName.text.toString()
            viewModel.setBookmark(bookmark)
            Toast.makeText(requireContext(), getString(R.string.saved), Toast.LENGTH_SHORT).show()
            saved?.invoke(Any())
            dismiss()
        }

        edit.setOnClickListener {
            editName.requestFocus()
            editName.setSelection(editName.text.toString().length)
            showKeyboard(editName)
        }
    }
}