package com.horlock.yandextest.utils

import android.content.Context
import android.content.res.Resources
import android.util.DisplayMetrics
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.PermissionChecker
import androidx.fragment.app.commit
import com.horlock.yandextest.R
import com.horlock.yandextest.base.BaseFragment
import kotlinx.android.synthetic.main.activity_main.view.*

fun hideKeyboard(view: View?) {
    if (view != null) {
        val imm = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}

fun showKeyboard(view: View?) {
    if (view != null) {
        val imm = view.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
    }
}

fun AppCompatActivity.makeFullScreen() {
    window.setFlags(
        WindowManager.LayoutParams.FLAG_FULLSCREEN,
        WindowManager.LayoutParams.FLAG_FULLSCREEN
    )
}

fun AppCompatActivity.isGranted(permission: String) = run {
        (PermissionChecker.checkSelfPermission(this, permission
        ) == PermissionChecker.PERMISSION_GRANTED)
}

fun AppCompatActivity.replaceFragment(fragment: BaseFragment, addBackStack: Boolean = false,
                                      @IdRes id: Int = R.id.container, tag : String = "base_fragment_tag") {
    supportFragmentManager.commit {
        if(addBackStack) addToBackStack(fragment.hashCode().toString())
        replace(id, fragment)
    }
}

fun Context.convertDpToPixel(dp: Float): Int {
    val metrics = resources.displayMetrics
    return (dp * (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)).toInt()
}
