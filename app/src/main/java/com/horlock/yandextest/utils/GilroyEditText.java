package com.horlock.yandextest.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;

public class GilroyEditText extends AppCompatEditText {
    public GilroyEditText(Context context) {
        super(context);
        setFont(context);
    }

    public GilroyEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont(context);
    }

    public GilroyEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFont(context);
    }

    private void setFont(Context context) {
        setTypeface(Typeface.createFromAsset(context.getResources().getAssets(), "font/Gilroy-Light.otf"));
    }
}

